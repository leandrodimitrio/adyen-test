<?php

namespace App\Http\Controllers;

use Session;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

class AdyenController
{
    private $pspReference = NULL;
    private $resultCode = NULL;
    private $authCode = NULL;

    public function authorise() {
        if (Session::has('pspReference')) {
            $this->pspReference = Session::get('pspReference');
            //echo "Authorization already exists. Reference # $this->pspReference";
            return view("welcome",['content' => "Authorization already exists. Reference # $this->pspReference"]);
        } else {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('POST','https://pal-test.adyen.com/pal/servlet/Payment/v50/authorise',[
                'auth' =>  ['ws_457345@Company.AdyenRecruitment', 'rRS{(3)X%-d^~fAhh]FVk!?+n'],
                'form_params' => [
                        'merchantAccount' => 'AdyenRecruitmentCOM',
                        'merchantReference' => 'Leandro Dimitrio Silva',
                        'card.number' => '5413330089999990',
                        'card.expiryMonth' => '02',
                        'card.expiryYear' => '2028',
                        'card.cvc' => '737',
                        'card.holderName' => 'John Smith',
                        'amount.value' =>  '100',
                        'amount.currency' => 'USD',
                        'reference' => md5('authorise')
                    ]
                ]);

            if($response->getStatusCode() == "200") {
                $result = explode("&",$response->getBody());
    
                $pspReference = explode("=",$result[0]);
                $resultCode = explode("=",$result[1]);
    
                if ($resultCode[1] == "Authorised") {
                    Session::put('pspReference', $pspReference[1]);
                    $pspReference = $pspReference[1];
                    return view("welcome",['content' => "Authorization successful. Reference # $pspReference"]);
                }
                else {
                    return view("welcome",['content' => "Wrong code. Try again."]);
                }
            }
            else {
                return view("welcome",['content' => "Authorisation error. Try again."]);
            }
        }
    }

    public function capture()
    {
        if (Session::has('pspReference')) {
            $this->pspReference = Session::get('pspReference');
        } else {
            $this->pspReference = $this->authorise();
        }

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST','https://pal-test.adyen.com/pal/servlet/Payment/v50/capture',[
            'auth' =>  ['ws_457345@Company.AdyenRecruitment', 'rRS{(3)X%-d^~fAhh]FVk!?+n'],
            'form_params' => [
                    'originalReference' => $this->pspReference,
                    'modificationAmount.value' => '50',
                    'modificationAmount.currency' => 'USD',
                    'merchantAccount' => 'AdyenRecruitmentCOM',
                    'reference' => md5('capture')
                ]
            ]);

        if($response->getStatusCode() == "200") {
            $captureResult = explode("&",$response->getBody());
    
            $capturePspReference = explode("=",$captureResult[0]);
            $captureResultCode = explode("=",$captureResult[1]);

            if ($captureResultCode[1] == "%5Bcapture-received%5D") {
                Session::put('capturePspReference', $capturePspReference[1]);
                return view("welcome",['content' => "Capture operation was successful. Original Reference # $this->pspReference | Capture Reference # $capturePspReference[1]"]);
            }
            else {
                return view("welcome",['content' => "Wrong capture code. Try again."]);
            }
        } else {
            return view("welcome",['content' => "Capture was not successful. Try again."]);
        }
    }
    public function refund()
    {
        if (Session::has('pspReference')) {
        $this->pspReference = Session::get('pspReference');

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST','https://pal-test.adyen.com/pal/servlet/Payment/v50/refund',[
            'auth' =>  ['ws_457345@Company.AdyenRecruitment', 'rRS{(3)X%-d^~fAhh]FVk!?+n'],
            'form_params' => [
                    'originalReference' => $this->pspReference,
                    'modificationAmount.value' => '100',
                    'modificationAmount.currency' => 'USD',
                    'merchantAccount' => 'AdyenRecruitmentCOM',
                    'reference' => md5('refund')
                ]
            ]);

            if($response->getStatusCode() == "200") {
                $refundResult = explode("&",$response->getBody());
        
                $refundPspReference = explode("=",$refundResult[0]);
                $refundResultCode = explode("=",$refundResult[1]);
    
                if ($refundResultCode[1] == "%5Brefund-received%5D") {
                    Session::put('refundPspReference', $refundPspReference[1]);
                    return view("welcome",['content' => "Refund operation was successful. Original Reference # $this->pspReference | Refund Reference # $refundPspReference[1]"]);
                }
                else {
                    return view("welcome",['content' => "Wrong refund code. Try again."]);
                }
            } else {
                return view("welcome",['content' => "Refund was not successful. Try again."]);
            }
        } else {
            return view("welcome",['content' => "No capture was identified. Try capturing again."]);
        }
    }
}